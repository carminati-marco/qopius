import { Component, OnInit } from '@angular/core';
import { WebSocketService } from './web-socket.service';
import { ImageService } from './images.services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Shaded images app';

  constructor(private webSocketService: WebSocketService, private imageService: ImageService) { }

  ngOnInit() {
    this.webSocketService.listen('toggle-property').subscribe(data => this.imageService.changeProperty(data));
  }
}
