export class Image {
  // tslint:disable-next-line
  _id: { $oid: string };
  url: string;
  shaded: boolean;
  deleted: boolean;
}
