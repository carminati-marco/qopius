import { Component, OnInit, OnDestroy } from '@angular/core';
import { Image } from './image';
import { ImageService } from '../images.services';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit, OnDestroy {
  images: Image[];
  subscription: any;

  constructor(
    private imageService: ImageService,
  ) {
    this.subscription = imageService.shadedChange.subscribe(value => {
      this.images
        .filter(im => im._id.$oid === value.id.$oid)
        .map(im => {
          im[value.property] = value.status;
        });
    });
  }

  ngOnInit() {
    this.getImages();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onToggleShaded(image: Image): void {
    this.imageService.toggleShaded(image).subscribe();
  }

  onToggleDeleted(image: Image): void {
    this.imageService.toggleDeleted(image).subscribe();
  }

  getImages(): void {
    this.imageService
      .getImages()
      .subscribe(images => (this.images = images.data as Image[]));
  }
}
