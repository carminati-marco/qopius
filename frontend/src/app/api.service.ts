import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    public getNews() {
        return this.httpClient.get(`http://localhost:5000/image`);
    }

  constructor(private httpClient: HttpClient) { }
}
