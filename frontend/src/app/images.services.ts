import { Injectable } from '@angular/core';

import { Observable, of, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Image } from './images/image';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  images: Image[] = [];
  shadedChange: Subject<any> = new Subject<any>();

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  private imagesUrl = 'http://localhost:5000/images';
  private toggleStatusUrl = 'http://localhost:5000/image/toggle-status';

  constructor(
    private http: HttpClient,
  ) {}

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`Image: ${message}`);
  }

  // todo: replace any taking the fetched image[] (check the ws).
  getImages(): Observable<any> {
    console.log('ImageService: fetched images');
    return this.http.get<any>(this.imagesUrl);
  }

  toggleShaded(image: Image): Observable<any> {
    return this.http.put(this.toggleStatusUrl, {id: image._id, status: 'shaded'}, this.httpOptions).pipe(
      tap(_ => this.log(`shaded image id=${image._id}`)),
      catchError(this.handleError<any>('toggle Shaded'))
    );
  }

  toggleDeleted(image: Image): Observable<any> {
    return this.http.put(this.toggleStatusUrl, {id: image._id, status: 'deleted'}, this.httpOptions).pipe(
      tap(_ => this.log(`deleted image id=${image._id}`)),
      catchError(this.handleError<any>('toggle Deleted'))
    );
  }

  changeProperty(data) {
    this.shadedChange.next(data);
  }
}
