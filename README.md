# Managing Images with Flask and AngularJS

## Infrastructure
The project is created in **Docker** and has the follow container:
- qopius-backend: in python/flask, uses gunicorn as server.
- qopius-frontend: in Angular JS.
- qopius-mongodb: container to save data.

## How to install

Requirement: **docker** and **docker-compose**
Just launch the command 
`make ultimate
`
from the root of the project; docker images and containers will be created.
If you need to remove the container, use `make clean`

## Use the project
The project manage the status of several images; the current state of the art enable **to shade** the images or to delete them.

**To load some image, please GET/open in a browser the follow URL
http://localhost:5000/insert-images
** 
(see **TODO List** later)

You can find the backend server working on http://localhost:5000/ and the customer client in http://localhost:4200/
Once open the customer client you could:
- shade an image: clicking over it, you should see the shade effect appears/disappears
- delete an image: removing from your images list.

## TODO list
- Automatic import of images.
- A POST endpoint to add images.
- Unit tests.
- Authentication system (allowing to show which user shaded which image, etc.).
- Check and improve PUT/GET methods in Frontend.  

