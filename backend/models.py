from flask_mongoengine import MongoEngine

db = MongoEngine()


class Image(db.Document):
    url = db.StringField()
    shaded = db.BooleanField(default=False)
    deleted = db.BooleanField(default=False)

    def __str__(self):
        return self.url
