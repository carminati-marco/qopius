import os
from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_socketio import SocketIO, emit
from flask_mongoengine import MongoEngine
from models import Image


application = Flask(__name__)

application.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(application, logger=True,
                    engineio_logger=True, cors_allowed_origins="*")
CORS(application)

application.config['MONGODB_DB'] = 'qopius'
application.config['MONGODB_HOST'] = os.environ["DB_PORT_27017_TCP_ADDR"]
db = MongoEngine()
db.init_app(application)


@application.route('/')
def index():
    return jsonify(
        status=True,
        message='Live!'
    )


@application.route('/images')
def images():
    data = Image.objects.filter(deleted=False)
    return jsonify(status=True, data=data)


@application.route('/image/toggle-status', methods=['PUT'])
def toggle_status():
    req_data = request.get_json()
    _id = req_data['id']

    image = Image.objects.get_or_404(id=_id.get('$oid'))
    if image is not None:
        _status = req_data['status']
        new_status_value = not getattr(image, _status)
        setattr(image, _status, new_status_value)
        image.save()

        emit("toggle-property",
             {'id': _id, 'property': _status, 'status': new_status_value},
             broadcast=True, namespace='/')

    return jsonify(status=True, result="ok")


@application.route('/insert-images')
def insert_images():
    Image.objects.all().delete()

    Image(url="http://127.0.0.1:5000/static/-hjiaijjcjbebgj-DxB--mzLTGm_LvXdVW_dZhdC6JzCd.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiaijjcjbebgj-DxB--mzLTGm_LvXdVW_ikDkSYbJTt.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiaijjcjbebha-JAa--mzLTGm_LvXdVW_BfFvj9B878.jpg", shaded=True).save()
    Image(url="http://127.0.0.1:5000/static/-hjiaijjcjbebha-JAa--mzLTGm_LvXdVW_cFeVo3xbJR.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiaijjcjbebha-JAa--mzLTGm_LvXdVW_OFwGtoySF6.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiaijjcjbebha-JAa--mzLTGm_LvXdVW_UzPGgAMLIO.jpg", shaded=True).save()
    Image(url="http://127.0.0.1:5000/static/-hjiaijjcjbebhc-jO4--mzLTGm_LvXdVW_qHqd2rgjgc.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiaijjcjbebhh-ZPE--mzLTGm_LvXdVW_t4kWy7BEM4.jpg", shaded=True).save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijecge-Uir--En9eLw_LvXdVW_EN1aqiqBNa_clean.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijecge-Uir--En9eLw_LvXdVW_Fe6RXnZ3or_clean.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijecge-Uir--En9eLw_LvXdVW_pUByeDXpKV_clean.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijecge-Uir--En9eLw_LvXdVW_YB1imkjSky_clean.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijecgj-MT0--En9eLw_LvXdVW_rX58Hsy3cy_clean.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijechg-LBR--En9eLw_LvXdVW_KywwqfBfuu_clean.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijechg-LBR--En9eLw_LvXdVW_PyQKaGYfIe_clean.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijechg-LBR--En9eLw_LvXdVW_q5bznmP9sc_clean.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijecib-Flf--En9eLw_LvXdVW_uqhdjGni9a_clean.jpg").save()
    Image(url="http://127.0.0.1:5000/static/-hjiajbjdijecib-Flf--En9eLw_LvXdVW_yafehgkRed_clean.jpg", shaded=True).save()

    return jsonify(status=True, result="ok")


if __name__ == "__main__":
    ENVIRONMENT_DEBUG = os.environ.get("APP_DEBUG", True)
    ENVIRONMENT_PORT = os.environ.get("APP_PORT", 5000)
    socketio.run(application, host='0.0.0.0',
                 port=ENVIRONMENT_PORT, debug=ENVIRONMENT_DEBUG)
